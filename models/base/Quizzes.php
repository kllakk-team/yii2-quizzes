<?php

namespace kllakk\quizzes\models\base;

use Yii;

/**
 * This is the model class for table "quizzes".
 *
 * @property int $quiz_id
 * @property string $quiz_name
 * @property string $message_before
 * @property string $message_after
 * @property string $message_comment
 * @property string $message_end_template
 * @property string $user_email_template
 * @property string $admin_email_template
 * @property string $submit_button_text
 * @property string $name_field_text
 * @property string $business_field_text
 * @property string $email_field_text
 * @property string $phone_field_text
 * @property string $comment_field_text
 * @property string $email_from_text
 * @property string $question_answer_template
 * @property string $leaderboard_template
 * @property int $system
 * @property int $randomness_order
 * @property int $logged_in_user_contact
 * @property int $show_score
 * @property int $send_user_email
 * @property int $send_admin_email
 * @property int $contact_info_location
 * @property int $user_name
 * @property int $user_comp
 * @property int $user_email
 * @property int $user_phone
 * @property string $admin_email
 * @property int $comment_section
 * @property int $question_from_total
 * @property int $total_user_tries
 * @property string $total_user_tries_text
 * @property string $certificate_template
 * @property int $social_media
 * @property string $social_media_text
 * @property int $pagination
 * @property string $pagination_text
 * @property int $timer_limit
 * @property string $quiz_style
 * @property int $question_numbering
 * @property string $quiz_settings
 * @property string $theme_selected
 * @property string $last_activity
 * @property int $require_log_in
 * @property string $require_log_in_text
 * @property int $limit_total_entries
 * @property string $limit_total_entries_text
 * @property string $scheduled_timeframe
 * @property string $scheduled_timeframe_text
 * @property int $disable_answer_onselect
 * @property int $ajax_show_correct
 * @property int $quiz_views
 * @property int $quiz_taken
 * @property int $deleted
 * @property string $quiz_description
 */
class Quizzes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quizzes_quizzes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quiz_name'], 'required'],
            [['quiz_name', 'message_before', 'message_after', 'message_comment', 'message_end_template', 'user_email_template', 'admin_email_template', 'submit_button_text', 'name_field_text', 'business_field_text', 'email_field_text', 'phone_field_text', 'comment_field_text', 'email_from_text', 'question_answer_template', 'leaderboard_template', 'admin_email', 'total_user_tries_text', 'certificate_template', 'social_media_text', 'pagination_text', 'quiz_style', 'quiz_settings', 'theme_selected', 'require_log_in_text', 'limit_total_entries_text', 'scheduled_timeframe', 'scheduled_timeframe_text', 'quiz_description'], 'string'],
            [['system', 'randomness_order', 'logged_in_user_contact', 'show_score', 'send_user_email', 'send_admin_email', 'contact_info_location', 'user_name', 'user_comp', 'user_email', 'user_phone', 'comment_section', 'question_from_total', 'total_user_tries', 'social_media', 'pagination', 'timer_limit', 'question_numbering', 'require_log_in', 'limit_total_entries', 'disable_answer_onselect', 'ajax_show_correct', 'quiz_views', 'quiz_taken', 'deleted'], 'integer'],
            [['last_activity'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'quiz_id' => 'Quiz ID',
            'quiz_name' => 'Quiz Name',
            'message_before' => 'Message Before',
            'message_after' => 'Message After',
            'message_comment' => 'Message Comment',
            'message_end_template' => 'Message End Template',
            'user_email_template' => 'User Email Template',
            'admin_email_template' => 'Admin Email Template',
            'submit_button_text' => 'Submit Button Text',
            'name_field_text' => 'Name Field Text',
            'business_field_text' => 'Business Field Text',
            'email_field_text' => 'Email Field Text',
            'phone_field_text' => 'Phone Field Text',
            'comment_field_text' => 'Comment Field Text',
            'email_from_text' => 'Email From Text',
            'question_answer_template' => 'Question Answer Template',
            'leaderboard_template' => 'Leaderboard Template',
            'system' => 'System',
            'randomness_order' => 'Randomness Order',
            'logged_in_user_contact' => 'Logged In User Contact',
            'show_score' => 'Show Score',
            'send_user_email' => 'Send User Email',
            'send_admin_email' => 'Send Admin Email',
            'contact_info_location' => 'Contact Info Location',
            'user_name' => 'User Name',
            'user_comp' => 'User Comp',
            'user_email' => 'User Email',
            'user_phone' => 'User Phone',
            'admin_email' => 'Admin Email',
            'comment_section' => 'Comment Section',
            'question_from_total' => 'Question From Total',
            'total_user_tries' => 'Total User Tries',
            'total_user_tries_text' => 'Total User Tries Text',
            'certificate_template' => 'Certificate Template',
            'social_media' => 'Social Media',
            'social_media_text' => 'Social Media Text',
            'pagination' => 'Pagination',
            'pagination_text' => 'Pagination Text',
            'timer_limit' => 'Timer Limit',
            'quiz_style' => 'Quiz Style',
            'question_numbering' => 'Question Numbering',
            'quiz_settings' => 'Quiz Settings',
            'theme_selected' => 'Theme Selected',
            'last_activity' => 'Last Activity',
            'require_log_in' => 'Require Log In',
            'require_log_in_text' => 'Require Log In Text',
            'limit_total_entries' => 'Limit Total Entries',
            'limit_total_entries_text' => 'Limit Total Entries Text',
            'scheduled_timeframe' => 'Scheduled Timeframe',
            'scheduled_timeframe_text' => 'Scheduled Timeframe Text',
            'disable_answer_onselect' => 'Disable Answer Onselect',
            'ajax_show_correct' => 'Ajax Show Correct',
            'quiz_views' => 'Quiz Views',
            'quiz_taken' => 'Quiz Taken',
            'deleted' => 'Deleted',
            'quiz_description' => 'Quiz Description',
        ];
    }

    /**
     * {@inheritdoc}
     * @return QuizzesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new QuizzesQuery(get_called_class());
    }
}

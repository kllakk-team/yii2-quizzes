<?php


namespace kllakk\quizzes\models\base;

use Yii;

/**
 * This is the model class for table "results".
 *
 * @property int $result_id
 * @property int $quiz_id
 * @property string $quiz_name
 * @property int $quiz_system
 * @property int $point_score
 * @property int $correct_score
 * @property int $correct
 * @property int $total
 * @property string $name
 * @property string $business
 * @property string $email
 * @property string $phone
 * @property int $user
 * @property string $user_ip
 * @property string $time_taken
 * @property string $time_taken_real
 * @property string $quiz_results
 * @property int $deleted
 */
class Results extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quizzes_results';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quiz_id'], 'required'],
            [['quiz_id', 'quiz_system', 'point_score', 'correct_score', 'correct', 'total', 'user', 'deleted'], 'integer'],
            [['time_taken_real'], 'safe'],
            [['quiz_results'], 'string'],
            [['quiz_name', 'name', 'business', 'email', 'phone', 'user_ip', 'time_taken'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'result_id' => 'Result ID',
            'quiz_id' => 'Quiz ID',
            'quiz_name' => 'Quiz Name',
            'quiz_system' => 'Quiz System',
            'point_score' => 'Point Score',
            'correct_score' => 'Correct Score',
            'correct' => 'Correct',
            'total' => 'Total',
            'name' => 'Name',
            'business' => 'Business',
            'email' => 'Email',
            'phone' => 'Phone',
            'user' => 'User',
            'user_ip' => 'User Ip',
            'time_taken' => 'Time Taken',
            'time_taken_real' => 'Time Taken Real',
            'quiz_results' => 'Quiz Results',
            'deleted' => 'Deleted',
        ];
    }
}


<?php


namespace kllakk\quizzes\models\base;

use Yii;

/**
 * This is the model class for table "questions".
 *
 * @property int $question_id
 * @property int $quiz_id
 * @property string $question_name
 * @property string $answer_array
 * @property string $answer_one
 * @property int $answer_one_points
 * @property string $answer_two
 * @property int $answer_two_points
 * @property string $answer_three
 * @property int $answer_three_points
 * @property string $answer_four
 * @property int $answer_four_points
 * @property string $answer_five
 * @property int $answer_five_points
 * @property string $answer_six
 * @property int $answer_six_points
 * @property int $correct_answer
 * @property string $question_answer_info
 * @property int $comments
 * @property string $hints
 * @property int $question_order
 * @property int $question_type
 * @property string $question_type_new
 * @property string $question_settings
 * @property string $category
 * @property int $deleted
 */
class Questions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quizzes_questions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['quiz_id', 'question_name'], 'required'],
            [['quiz_id', 'answer_one_points', 'answer_two_points', 'answer_three_points', 'answer_four_points', 'answer_five_points', 'answer_six_points', 'correct_answer', 'comments', 'question_order', 'question_type', 'deleted'], 'integer'],
            [['answer_array'], 'string'],
            [['question_name', 'answer_one', 'answer_two', 'answer_three', 'answer_four', 'answer_five', 'answer_six', 'question_answer_info', 'hints', 'question_type_new', 'question_settings', 'category'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'question_id' => 'Question ID',
            'quiz_id' => 'Quiz ID',
            'question_name' => 'Question Name',
            'answer_array' => 'Answer Array',
            'answer_one' => 'Answer One',
            'answer_one_points' => 'Answer One Points',
            'answer_two' => 'Answer Two',
            'answer_two_points' => 'Answer Two Points',
            'answer_three' => 'Answer Three',
            'answer_three_points' => 'Answer Three Points',
            'answer_four' => 'Answer Four',
            'answer_four_points' => 'Answer Four Points',
            'answer_five' => 'Answer Five',
            'answer_five_points' => 'Answer Five Points',
            'answer_six' => 'Answer Six',
            'answer_six_points' => 'Answer Six Points',
            'correct_answer' => 'Correct Answer',
            'question_answer_info' => 'Question Answer Info',
            'comments' => 'Comments',
            'hints' => 'Hints',
            'question_order' => 'Question Order',
            'question_type' => 'Question Type',
            'question_type_new' => 'Question Type New',
            'question_settings' => 'Question Settings',
            'category' => 'Category',
            'deleted' => 'Deleted',
        ];
    }
}


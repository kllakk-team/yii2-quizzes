<?php

namespace kllakk\quizzes\models\base;

/**
 * This is the ActiveQuery class for [[Quizzes]].
 *
 * @see Quizzes
 */
class QuizzesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return Quizzes[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Quizzes|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

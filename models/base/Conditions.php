<?php


namespace kllakk\quizzes\models\base;

use Yii;

/**
 * This is the model class for table "conditions".
 *
 * @property int $question_id
 * @property int $quiz_id
 * @property string $condition_array
 * @property int $deleted
 */
class Conditions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'quizzes_conditions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question_id', 'quiz_id'], 'required'],
            [['question_id', 'quiz_id', 'deleted'], 'integer'],
            [['condition_array'], 'string'],
            [['question_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'question_id' => 'Question ID',
            'quiz_id' => 'Quiz ID',
            'condition_array' => 'Condition Array',
            'deleted' => 'Deleted',
        ];
    }
}


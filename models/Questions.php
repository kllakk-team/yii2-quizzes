<?php

namespace kllakk\quizzes\models;

class Questions extends \kllakk\quizzes\models\base\Questions
{
    public function getAnswers()
    {
        return json_decode($this->answer_array);
    }

    /**
     * {@inheritdoc}
     * @return QuestionsQuery the active query used by this AR class.
     */
    public static function find()
    {
        $query = new QuestionsQuery(get_called_class());
        $query->orderBy(['question_order' => SORT_ASC, 'question_id' => SORT_ASC]);
        return $query;
    }
}

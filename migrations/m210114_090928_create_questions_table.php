<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%quizzes_questions}}`.
 */
class m210114_090928_create_questions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%quizzes_questions}}', [
            'question_id' => $this->primaryKey(),
            'quiz_id' => $this->integer()->notNull(),
            'question_name' => $this->string()->notNull(),
            'answer_array' => $this->text(),
            'answer_one' => $this->string(),
            'answer_one_points' => $this->integer(),
            'answer_two' => $this->string(),
            'answer_two_points' => $this->integer(),
            'answer_three' => $this->string(),
            'answer_three_points' => $this->integer(),
            'answer_four' => $this->string(),
            'answer_four_points' => $this->integer(),
            'answer_five' => $this->string(),
            'answer_five_points' => $this->integer(),
            'answer_six' => $this->string(),
            'answer_six_points' => $this->integer(),
            'correct_answer' => $this->integer(),
            'question_answer_info' => $this->string(),
            'comments' => $this->integer(),
            'hints' => $this->string(),
            'question_order' => $this->integer(),
            'question_type' => $this->integer(),
            'question_type_new' => $this->string(),
            'question_settings' => $this->string(),
            'category' => $this->string(),
            'deleted' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%quizzes_questions}}');
    }
}

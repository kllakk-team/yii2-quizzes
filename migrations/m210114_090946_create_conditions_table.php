<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%quizzes_conditions}}`.
 */
class m210114_090946_create_conditions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%quizzes_conditions}}', [
            'question_id' => $this->integer()->unique()->notNull(),
            'quiz_id' => $this->integer()->notNull(),
            'condition_array' => $this->text(),
            'deleted' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%quizzes_conditions}}');
    }
}

<?php

namespace kllakk\quizzes;

use yii\helpers\Url;

/**
 * quizzes module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'kllakk\quizzes\controllers';

    /**
     * @var string Upload Directory
     */
    public $uploadDirectory;

    /**
     * @var string Public Directory
     */
    public $publicDirectory;

    /**
     * @var string Email From
     */
    public $mailFrom;

    public function getApiRoutes()
    {
        return [
            'quizzes/import'    => Url::toRoute(['quizzes/import'], true),
            'quizzes/create'    => Url::toRoute(['quizzes/create'], true),
            'quizzes/get'       => Url::toRoute(['quizzes/get'], true),
            'quizzes/update'    => Url::toRoute(['quizzes/update'], true),
            'quizzes/delete'    => Url::toRoute(['quizzes/delete'], true),
            'quizzes/export'    => Url::toRoute(['quizzes/export', 'id' => 'id'], true),
            'quizzes/list'      => Url::toRoute(['quizzes/list'], true),

            'questions/create'  => Url::toRoute(['questions/create'], true),
            'questions/get'     => Url::toRoute(['questions/get'], true),
            'questions/list'    => Url::toRoute(['questions/list'], true),
            'questions/delete'  => Url::toRoute(['questions/delete'], true),
            'questions/update'  => Url::toRoute(['questions/update', 'id' => 'id'], true),
            'questions/reorder' => Url::toRoute(['questions/reorder'], true),

            'conditions/list'    => Url::toRoute(['conditions/list'], true),
            'conditions/update'  => Url::toRoute(['conditions/update'], true),

            'results/list'  => Url::toRoute(['results/list', 'page' => 'page', 'size' => 'size'], true),
        ];
    }
}

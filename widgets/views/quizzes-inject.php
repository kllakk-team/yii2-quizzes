<?php

use kllakk\quizzes\widgets\QuizButton;

/* @var $quizzes Quizzes */

foreach ($quizzes as $quiz) {
    echo QuizButton::widget(['quizId' => $quiz->quiz_id]);
}

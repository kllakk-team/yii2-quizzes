<?php

namespace kllakk\quizzes\widgets;

use kllakk\quizzes\models\Quizzes;
use yii\helpers\Html;
use yii\helpers\Url;

class QuizButton extends \yii\base\Widget
{
    public $quizId;
    public $buttonText;
    public $resultButtonText;
    public $buttonClass;

    public function run()
    {
        $quiz = Quizzes::findOne($this->quizId);
        $submitUrl = Url::toRoute(['quizzes/results/submit'], true);

        return $this->render('quiz-button', [
            'resultButtonText' => $this->resultButtonText,
            'buttonText' => $this->buttonText,
            'buttonClass' => $this->buttonClass,
            'quiz' => $quiz,
            'submitUrl' => $submitUrl,
        ]);
    }
}

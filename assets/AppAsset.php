<?php

namespace kllakk\quizzes\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle
{
    public $depends = [
        'yii\web\JqueryAsset',
        'kllakk\quizzes\assets\VueQuizComponentsAsset',
    ];
}

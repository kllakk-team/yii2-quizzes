<?php

namespace kllakk\quizzes\assets;

use yii\web\AssetBundle;

class FontAwesomeAsset extends AssetBundle
{
    public $css = [
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css'
    ];

    public $js = [
        'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js'
    ];
}

<?php

namespace kllakk\quizzes\assets;

use yii\web\AssetBundle;

class VueAsset extends AssetBundle
{
    public $js = [
        'https://unpkg.com/vue@2.6.12/dist/vue.js',
    ];
}

const App = {
  template: `
    <b-container fluid>
        <b-row>
            <b-col>
                <h1>{{ title }}</h1>
            </b-col>
        </b-row>
        <b-row>
            <b-col>
                <quizzes v-if="Object.keys(routes).length > 0" :routes="routes" />
            </b-col>
        </b-row>
    </b-container>
    `,
  data: () => {
    return {
      routes: []
    }
  },
  components: { Quizzes },
  methods: {
    getRoutes() {
      $.getJSON(this.AllRoutesLink).done((response) => {
        this.routes = response;
      });
    },
  },
  created() {
    this.getRoutes();
  },
  props: ['title', 'AllRoutesLink']
};

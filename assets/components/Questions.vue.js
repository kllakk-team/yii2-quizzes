const Questions = {
  template: `
    <b-container fluid class="px-0">
        <b-row class="mb-3">
            <b-col>
                <question-update :routes="routes" :quiz-id="QuizId" @created="questionUpdated" />
            </b-col>
        </b-row>
        <b-row class="mb-3">
            <b-col>
                <b-table :busy="isBusy" v-sortable="sortableOptions" striped hover :items="items" :fields="fields">
                    <template #table-busy>
                      <div class="text-center text-danger my-2">
                        <b-spinner class="align-middle"></b-spinner>
                        <strong>Loading...</strong>
                      </div>
                    </template>
                    <template v-slot:cell(actions)="data">
                        <b-button v-b-modal="'modal-question-conditions-' + data.item.question_id">
                            <i class="fab fa-buffer"></i>
                            <conditions :routes="routes" :quiz-id="QuizId" :question-id="data.item.question_id" />
                        </b-button>
                        <b-button v-b-modal="'modal-question-update-' + data.item.question_id">
                            <i class="fas fa-edit"></i>
                            <question-update :routes="routes" :quiz-id="QuizId" @updated="questionUpdated" :id="data.item.question_id" />
                        </b-button>
                        <b-button @click="deleteQuestion(data.item.question_id)"><i class="far fa-trash-alt"></i></b-button>
                    </template>
                </b-table>
            </b-col>
        </b-row>
    </b-container>
    `,
  components: { Conditions, QuestionUpdate },
  directives: {
    sortable: {
      bind(el, binding, vnode) {
        const table = el;
        const options = binding.value;
        table._sortable = Sortable.create(table.querySelector("tbody"), { ...options });
      }
    }
  },
  methods: {
    getQuestions() {
      $.getJSON(this.routes['questions/list'], { quizId: this.QuizId }).done((response) => {
        this.items = response;
        this.isBusy = false;
      });
    },
    deleteQuestion(QuestionId) {
      this.$bvModal.msgBoxConfirm('Подтверждаете удаление?!', {
        title: `Удалить вопрос №${QuestionId}`,
        okTitle: 'Удалить',
        centered: true,
        cancelTitle: 'Отмена',
      })
        .then(value => {
          if (value) {
            $.post(this.routes['questions/delete'], { id: QuestionId }).done((response) => {
              this.getQuestions();
            });
          }
        })
    },
    questionUpdated() {
      this.getQuestions();
    },
    moveArrayItem(input, from, to) {
      const elm = input.splice(from, 1)[0];
      input.splice(to, 0, elm);
    },
    onDragEnd(evt) {
      this.isBusy = true;

      let items = JSON.parse(JSON.stringify(this.items));

      // применить сортировку к массиву
      this.moveArrayItem(items, evt.oldIndex, evt.newIndex);

      // применить сортировку к базе!
      $.post(this.routes['questions/reorder'], {
        quizId: items[evt.oldIndex].quiz_id,
        questionsIds: items.map(item => {
          return item.question_id;
        }),
      }).done((response) => {
        this.getQuestions();
      });
    },
  },
  computed: {
    sortableOptions() {
      return {
        chosenClass: 'is-selected',
        onEnd: this.onDragEnd,
      }
    }
  },
  data: () => {
    return {
      isBusy: false,
      items: [],
      fields: [
        { key: 'question-id', label: '№', thStyle: { width: '50px' } },
        { key: 'question_name', label: 'Вопрос' },
        { key: 'actions', label: '', thStyle: { width: '200px' } },
      ]
    }
  },
  created() {
    this.getQuestions();
  },
  props: ['QuizId', 'routes']
};

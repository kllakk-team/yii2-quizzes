document.addEventListener("DOMContentLoaded", function () {
  Vue.component("App", App);
  new Vue({ el: "#quiz-app", components: { App }, });
});

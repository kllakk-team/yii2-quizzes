const Conditions = {
  template: `
<div>
        <b-modal @show="show" :id="modalId" centered size="md"
                 ok-title="Сохранить" cancel-title="Отменить"
                 @ok="update"
                 :title="'Условия для вопроса №' + QuestionId">
            <b-container fluid class="px-0">
                <b-row class="mb-3">
                    <b-col>
                        <b-button @click="insertCondition" variant="outline-secondary">Добавить</b-button>
                    </b-col>
                </b-row>
                <b-row class="mb-3">
                    <b-col>
                        <div v-for="(item, index) in items" :key="index" class="mb-3">
                            <b-card>
                                <b-card-text>
                                    <p class="mb-2 clearfix">
                                        Условие {{ index + 1 }}
                                        <b-button @click="deleteConditions(index)" class="float-right"><i class="far fa-trash-alt"></i></b-button>
                                    </p>
                                    <p>
                                        <b-form-select v-model="item.QuestionRelatedId" :options="listQuestions"></b-form-select>
                                    </p>
                                    <p>
                                        <b-form-select v-model="item.ConditionType" :options="types"></b-form-select>
                                    </p>
                                    <p>
                                        <b-form-select v-model="item.ConditionValue" :options="listAnswers(item.QuestionRelatedId)"></b-form-select>
                                    </p>
                                </b-card-text>
                            </b-card>
                        </div>
                    </b-col>
                </b-row>
            </b-container>
        </b-modal>
    </div>
    `,
  methods: {
    getConditions() {
      $.getJSON(this.routes['questions/get'], { quizId: this.QuizId }).done((response) => {
        this.questions = response;
        this.questions.forEach((question) => {
          question.answer_array = question.answer_array ? JSON.parse(question.answer_array) : []
        });
        $.getJSON(this.routes['conditions/list'], { quizId: this.QuizId, questionId: this.QuestionId }).done((response) => {
          this.items = response.condition_array ? JSON.parse(response.condition_array) : [];
        });
      });
    },
    insertCondition() {
      this.items.push(Object.assign({}, this.emptyCondition));
    },
    deleteConditions(index) {
      this.$bvModal.msgBoxConfirm('Подтверждаете удаление?!', {
        title: `Удалить условие №${index}`,
        okTitle: 'Удалить',
        centered: true,
        cancelTitle: 'Отмена',
      })
        .then(value => {
          if (value) {
            this.items = this.items.filter((v, i) => i !== index);
          }
        })
    },
    show() {
      this.getConditions();
    },
    update() {
      let params = {
        quiz_id: this.QuizId,
        question_id: this.QuestionId,
        condition_array: this.items
      };

      $.post(this.routes['conditions/update'], params ).done(( response ) => {
        this.$emit('updated');
        this.items = [];
        this.$bvModal.hide(this.modalId);
      });
    },
    listAnswers(questionId) {
      let result = [];

      if (this.questions) {
        let question = this.questions.find(question => question.question_id == questionId);
        if (question) {
          let answers = question.answer_array;
          if (answers) {
            result = answers.map((answer) => {
              return {
                value: answer.AnswerText,
                text: answer.AnswerText
              }
            });
          }
        }
      }

      return result;
    }
  },
  computed: {
    modalId() {
      return 'modal-question-conditions-' + this.QuestionId;
    },
    listQuestions() {
      return this.questions.map((question) => {
        return {
          value: question.question_id,
          text: question.question_name
        }
      });
    }
  },
  data: () => {
    return {
      types: [
        { value: 'equal', text: 'Равен' },
        { value: 'not-equal', text: 'Не равен' },
      ],
      emptyCondition: {
        QuestionRelatedId: null,
        ConditionType: null,
        ConditionValue: null,
      },
      items: [],
      questions: [],
    }
  },
  props: ['QuizId', 'QuestionId', 'routes']
};

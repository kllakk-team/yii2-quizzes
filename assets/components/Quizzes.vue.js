const Quizzes = {
  template: `
    <b-container fluid class="px-0">
        <b-row class="mb-3">
            <b-col>
                <b-container fluid class="px-0">
                    <b-row class="mt-3">
                        <b-col>
                            <b-button v-b-modal.modal-quiz-new variant="outline-primary">Добавить новый</b-button>
                            <b-button v-b-modal.modal-quiz-results variant="secondary">Результаты</b-button>
                            <quiz-new :routes="routes" @created="quizUpdated" />
                            <results :routes="routes"/>
                        </b-col>
                  </b-row>
              </b-container>
            </b-col>
        </b-row>
        <b-row class="mb-3">
            <b-col>
                <b-table striped hover :items="items" :fields="fields">
                    <template v-slot:cell(embed)="data">
                        <b-input-group>
                            <b-input-group-prepend>
                                <b-button @click="copyToClipboard('clp-quiz-' + data.item.quiz_id)">
                                    <i class="far fa-hand-point-up"></i>
                                </b-button>
                            </b-input-group-prepend>
                            <b-form-input type="text" :value="printQuizLink(data.item.quiz_id)" :id="'clp-quiz-' + data.item.quiz_id" />
                        </b-input-group>
                    </template>
                    <template v-slot:head(actions)="data">
                        <label class="btn m-0 btn-link py-0 px-0" for="import-quiz-file">Импорт <i class="fas fa-file-import"></i></label>
                        <div class="d-none">
                            <b-form-file accept=".json" @input="importQuiz" id="import-quiz-file" name="import-quiz-file" plain></b-form-file>
                        </div>
                    </template>
                    <template v-slot:cell(actions)="data">
                        <b-button v-b-modal="'modal-quiz-update-' + data.item.quiz_id">
                            <i class="fas fa-edit"></i>
                            <quiz-update :routes="routes" @updated="quizUpdated"  :id="data.item.quiz_id" />
                        </b-button>
                        <b-button @click="deleteQuiz(data.item.quiz_id)"><i class="far fa-trash-alt"></i></b-button>
                        <b-button @click="exportQuiz(data.item.quiz_id)"><i class="fas fa-download"></i></b-button>
                    </template>
                </b-table>
            </b-col>
        </b-row>
    </b-container>
    `,
  components: { QuizNew, QuizUpdate, Results },
  methods: {
    openPreview(quizId) {
      window.open(`preview/${quizId}`,'_blank');
    },
    printQuizLink(quizId) {
      return `<a href="#" data-quiz-id="${quizId}" class="qsm-modal-button">Нажмите здесь</a>`;
      //return `<?= \\kllakk\\quizzes\\widgets\\QuizButton::widget(['quizId' => ${quizId}, 'buttonText' => 'Нажмите здесь', 'buttonClass' => 'btn btn-primary']); ?>`;
    },
    copyToClipboard(elementId) {
      let copyText = document.getElementById(elementId);
      copyText.select();
      copyText.setSelectionRange(0, 99999); /*For mobile devices*/
      document.execCommand("copy");
    },
    getQuizzes() {
      $.getJSON(this.routes['quizzes/list']).done((response) => {
        this.items = response;
      });
    },
    deleteQuiz(QuizId) {
      this.$bvModal.msgBoxConfirm('Подтверждаете удаление?!', {
        title: `Удалить опрос №${QuizId}`,
        okTitle: 'Удалить',
        centered: true,
        cancelTitle: 'Отмена',
      })
        .then(value => {
          if (value) {
            $.post(this.routes['quizzes/delete'], { id: QuizId }).done((response) => {
              this.getQuizzes();
            });
          }
        })
    },
    importQuiz(file) {
      let formData = new FormData();
      formData.append(`file[]`, file, file.name);

      $.ajax({
        type: 'POST',
        enctype: 'multipart/form-data',
        url: this.routes['quizzes/import'],
        data: formData,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        success: (response) => {
          this.getQuizzes();
        }
      });
    },
    exportQuiz(QuizId) {
      window.open(this.routes['quizzes/export'].replace('id=id', `id=${QuizId}`), '_blank');
    },
    quizUpdated() {
      this.getQuizzes();
    }
  },
  data: () => {
    return {
      items: [],
      fields: [
        { key: 'quiz_id', label: '№', thStyle: { width: '50px' } },
        { key: 'quiz_name', label: 'Название' },
        { key: 'embed', label: '', thStyle: { width: '450px' } },
        { key: 'actions', label: '', thStyle: { width: '200px' } },
      ]
    }
  },
  created() {
    this.getQuizzes();
  },
  props: ['routes']
};

<?php

namespace kllakk\quizzes\assets;

use yii\web\AssetBundle;

class WidgetAsset extends AssetBundle
{
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    public $sourcePath = __DIR__ . '/widget';

    public $css = [
        'css/qmn_primary.css',
        'css/common.css',
        'css/jquery-ui.css',
        'css/modal.css',
        'css/jquery.ui.slider-rtl.css',
    ];

    public $js = [
        'js/underscore-min.js',
        'js/progressbar.min.js',
        //'js/jquery.js',
        'js/jquery-ui.js',
        'js/tooltip.min.js',
        'js/qsm-quiz.js',
        'js/jquery.inputmask.min.js',
    ];
}

<?php

use \yii\helpers\Url;

$this->title = 'Тесты/Опросы';
?>

<div class="quizzes-default-index">
    <div id="quiz-app">
        <App title="<?= $this->title ?>"
             all-routes-link="<?= Url::toRoute(['default/routes'], true) ?>"
        />
    </div>
</div>

<?php

namespace kllakk\quizzes\controllers;

use Yii;
use kllakk\quizzes\controllers\base\BaseController;
use kllakk\quizzes\models\Conditions;
use yii\filters\VerbFilter;

class ConditionsController extends BaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'update' => ['POST'],
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUpdate()
    {
        if ($quizId = Yii::$app->request->post('quiz_id')) {
            if ($questionId = Yii::$app->request->post('question_id')) {
                if (!($model = Conditions::findOne(['quiz_id' => $quizId, 'question_id' => $questionId]))) {
                    $model = new Conditions();
                }

                $model->quiz_id = $quizId;
                $model->question_id = $questionId;
                $conditionArray = Yii::$app->request->post('condition_array', []);
                $model->condition_array = json_encode($conditionArray);
                if ($model->save()) {
                    return $this->asJson($model);
                }
            }
        }
    }

    public function actionList($quizId, $questionId)
    {
        $model = Conditions::findOne(['quiz_id' => $quizId, 'question_id' => $questionId]);
        return $this->asJson($model);
    }
}
